import React from 'react'
import { GET_BOOKS } from '../type'

export default (state, action) => {
    const { payload, type } = action

    switch (type) {
        case GET_BOOKS:
            return {
                ...state,
                books: payload,
            }
        default:
            return state
            break;
    }

}