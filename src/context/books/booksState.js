import { useReducer } from 'react'
import booksReducer from './booksReducer'
import axios from 'axios'
import BooksContext from './booksContext'


const BooksState = (props) => {
    const initialState = {
        books: [],
    }
    const [state, dispatch] = useReducer(booksReducer, initialState)

    const getAllBooks = async () => {
        try {
            const url = await axios.get('api/books')
            const respuesta = await url.data
            dispatch({
                type: 'GET_BOOKS',
                payload: respuesta,
            })
        } catch (error) {
            console.error(error);
        }
    }

    return (
        <BooksContext.Provider
            value={{
                books: state.books,
                getAllBooks
            }}
        >
            {props.children}
        </BooksContext.Provider>
    )
}

export default BooksState