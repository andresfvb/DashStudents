import React, { useEffect, useContext, useState } from 'react';
import { Table, TableHeader, TableColumn, TableBody, TableRow, TableCell, User, Chip, Tooltip, getKeyValue, Button } from "@nextui-org/react";
import BooksContext from '@/src/context/books/booksContext';
import { EditIcon } from '@/public/image/Editcon';
import { DeleteIcon } from '@/public/image/DeleteIcon';

const TablesBooks = ({ setCarga, onOpen, setEdicion, carga }) => {
    const actualizar = (item) => {
        setEdicion(true)
        setCarga(item)
        onOpen()
    }
    const eliminar = async (item) => {
        await fetch('api/books', {
            method: 'DELETE',
            body: JSON.stringify(item),
        })
        setCarga({ name: '', editorial: '', author: '', cod: '', id: '' })
    }
    const statusColorMap = {
        disponible: "success",
        ocupado: "danger",
    };
    const renderCell = React.useCallback((user, columnKey) => {
        const cellValue = user[columnKey];

        switch (columnKey) {
            case "name":
                return (
                    <User
                        avatarProps={{ radius: "lg", src: user.avatar }}
                        description={user.email}
                        name={cellValue}
                    >
                        {user.email}
                    </User>
                );
            case "cod":
                return (
                    <div className="flex flex-col">
                        <p className="text-bold text-small capitalize">{cellValue}</p>

                    </div>
                );
            case "author":
                return (
                    <div className="flex flex-col">
                        <p className="capitalize" >
                            {cellValue}
                        </p>
                    </div>
                );
            case "state":
                return (
                    <Chip className="capitalize" color={statusColorMap[user.state]} size="sm" variant="flat">
                        {cellValue}
                    </Chip>
                );
            case "editorial":
                return (
                    <div className="flex flex-col">
                        <p className="capitalize" >
                            {cellValue}
                        </p>
                    </div>
                );
            case "actions":
                return (
                    <div className="relative flex items-center gap-5">
                        <Tooltip content="Edit user">
                            <span className="text-medium text-default-400 cursor-pointer active:opacity-50" onClick={() => actualizar(user)}>
                                <EditIcon />
                            </span>
                        </Tooltip>
                        <Tooltip color="danger" content="Delete user">
                            <span className="text-medium text-danger cursor-pointer active:opacity-50" onClick={() => eliminar(user)}>
                                <DeleteIcon />
                            </span>
                        </Tooltip>
                    </div>
                );
            default:
                return cellValue;
        }
    }, []);


    const { books, getAllBooks } = useContext(BooksContext)
    useEffect(() => {
        getAllBooks()
    }, [carga])
    const columns = [
        {
            uid: 'id',
            label: "#"
        },
        {
            uid: 'cod',
            label: "Codigo"
        },
        {
            uid: 'name',
            label: "Libro"
        },
        {
            uid: 'author',
            label: "Autor"
        },
        {
            uid: 'editorial',
            label: "Editorial"
        },
        {
            uid: 'state',
            label: "Disponibilidad"
        },
        {
            uid: 'actions',
            label: "Acciones"
        }
    ];

    const rows = [];
    books.forEach(element => {
        rows.push({ id: element.id, cod: element.codLibro, name: element.nameBook, author: element.autor, editorial: element.editorial, state: element.state })
    })
    console.log(books);
    return (
        <Table removeWrapper aria-label="Example table with custom cells">
            <TableHeader columns={columns}>
                {(column) => (
                    <TableColumn className="text-medium" key={column.uid} align={column.uid === "actions" ? "center" : "start"}>
                        {column.label}
                    </TableColumn>
                )}
            </TableHeader>
            <TableBody items={rows}>
                {(item) => (
                    <TableRow className="text-small" key={item.id}>
                        {(columnKey) => <TableCell>{renderCell(item, columnKey)}</TableCell>}
                    </TableRow>
                )}
            </TableBody>
        </Table>
    )
}

export default TablesBooks