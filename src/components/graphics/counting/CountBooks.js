import React from 'react'
import styles from '@/styles/graphics/countStudent.module.css'
import { Card, CardHeader, CardBody, CardFooter } from "@nextui-org/react";

const CountBooks = ({ datos }) => {
    return (
        <>
            {
                datos.map((elemento) => (
                    <Card key={elemento.id} className={`flex flex-col ${elemento.color}`}>
                        <CardHeader>
                            <h3 className='text-default-900'>{elemento.name}</h3>
                        </CardHeader>
                        <CardBody>
                            <p>{elemento.tamaño}</p>
                        </CardBody>
                    </Card>
                ))
            }
        </>

    )
}

export default CountBooks