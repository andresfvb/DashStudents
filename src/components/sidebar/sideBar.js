import React from 'react'
import Menu from './menu/Menu'
import styles from '@/styles/sidebar/sidebar.module.css'
import { Card, CardHeader, CardBody, CardFooter, Divider, Link, Image } from "@nextui-org/react"

const SideBar = () => {
    return (
        <aside className='flex flex gap-4 flex-wrap w-1/2 max-w-md h-screen'>
            <Card className='w-full' radius="none">
                <CardHeader className='flex gap-3 p-10'>
                    <Image src="/image/Logo-db.png" width={50} height={50} alt="Logotipo" radius="sm" />
                    <div className="flex flex-col">
                        <p className="text-md">DashBooks</p>
                        <p className="text-small text-default-500">dashbooks.org</p>
                    </div>
                </CardHeader>
                <Divider />
                <CardBody>
                    <Menu />
                </CardBody>
            </Card>
        </aside>

    )
}

export default SideBar