import React from 'react'
import Link from 'next/link'
import styles from '@/styles/sidebar/itemsMenu.module.css'
import { useRouter } from 'next/router';

const ItemsMenu = () => {
    const router = useRouter();
    const paginas = [
        { id: 1, path: '/', name: 'Dashboard' },
        { id: 2, path: '/booksList', name: 'Libros' },
        { id: 3, path: '/login', name: 'Login' },
    ]
    return (
        <div className={`flex flex-col gap-5 ${styles.itemsMenu}`}>
            {
                paginas.map(elemento => (
                    <Link href={elemento.path} key={elemento.id} ><div className={`rounded py-5 pl-5 ${router.pathname === elemento.path ? 'bg-primary-100' : ' hover:bg-default-100 bg-transparent'}`}>{elemento.name}</div></Link>

                ))
            }
            {/* <Link href="/" ><li className={`hover:bg-default-100 rounded py-5 ${styles[router.pathname === '/' ? 'active' : ' ']}`}>Dashboard</li></Link>
            <Link href="/booksList"><li className={`hover:bg-default-100 ${styles[router.pathname === '/booksList' ? 'active' : '']}`}>Libros</li></Link> */}
        </div>
    )
}

export default ItemsMenu