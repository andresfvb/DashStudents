import React from 'react'
import Head from 'next/head'
import Header from './Header'
import Footer from './Footer'
import { useRouter } from 'next/router';
const Layouts = ({ children, title = '', description = '' }) => {
    const router = useRouter();
    return (
        <>
            <Head>
                <title>{`DS - ${title}`}</title>
                <meta name='description' content={description}></meta>
            </Head>
            {
                router.pathname !== '/login' ? (
                    <div key="1" className="flex flex-col py-14 px-24 w-full">
                        {children}
                    </div>
                ) : (
                    <div key="2">
                        {children}

                    </div>
                )
            }

        </>
    )
}

export default Layouts