import React from 'react'
import Layouts from '../components/Layouts'
import GridLayout from "react-grid-layout";
import { Input } from "@nextui-org/react";
import { EyeFilledIcon } from '@/public/image/EyeFilledIcon';
import { EyeSlashFilledIcon } from '@/public/image/EyeSlashFilledIcon';
import { Card, CardHeader, CardBody, CardFooter, Divider, Link } from "@nextui-org/react"
import Image from 'next/image';
import { Button } from "@nextui-org/react";

const Login = () => {

    const [isVisible, setIsVisible] = React.useState(false);

    const toggleVisibility = () => setIsVisible(!isVisible);
    const login = async (e) => {
        e.preventDefault()
        const formData = {
            name: e.target.usuario.value,
            password: e.target.contraseña.value

        }
        await fetch('api/users', {
            method: 'POST',
            body: JSON.stringify(formData),
        })

    }
    return (
        <>
            <Layouts
                title={'Login'}
                description={'Inicio sesion'}
            >
                <div className='flex flex-row p-10 bg-white h-screen '>
                    <div className='flex flex-col logotipo basis-1/2 justify-center'>
                        <div className='flex gap-3 p-10'>
                            <Image src="/image/Logo-db.png" width={50} height={50} alt="Logotipo" radius="sm" />
                            <div className="flex flex-col">
                                <p className="text-default-500 "><b>DashBooks</b></p>
                                <p className="text-small text-default-500">La Biblioteca a tu <b>Alcance</b></p>
                            </div>
                        </div>
                        <Divider />
                        <Image className="w-screen" src="/image/background.svg" width={100} height={100} alt="Imagen del login" />

                    </div>
                    <div className='formulario basis-1/2 flex flex-col p-10 w-full justify-center items-center'>
                        <div className='text-black mb-4 w-full flex flex-col'>
                            <span className='text-small'>Conectate</span>
                            <h1 className='text-large'>Iniciar Sesion</h1>
                        </div>
                        <div className=' w-full flex'>
                            <form onSubmit={login} className='flex gap-4 flex-wrap w-full'>
                                <div className="flex w-full flex-wrap md:flex-nowrap gap-4">
                                    <Input
                                        isClearable
                                        type="text"
                                        label="Usuario"
                                        variant="bordered"
                                        placeholder=""

                                        onClear={() => console.log("input cleared")}
                                        className="max-w-xs"
                                        name="usuario"
                                    />
                                </div>
                                <div className="flex w-full flex-wrap md:flex-nowrap gap-4">
                                    <Input
                                        label="Password"
                                        variant="bordered"
                                        endContent={
                                            <button className="focus:outline-none" type="button" onClick={toggleVisibility}>
                                                {isVisible ? (
                                                    <EyeSlashFilledIcon className="text-2xl text-default-400 pointer-events-none" />
                                                ) : (
                                                    <EyeFilledIcon className="text-2xl text-default-400 pointer-events-none" />
                                                )}
                                            </button>
                                        }
                                        type={isVisible ? "text" : "password"}
                                        className="max-w-xs"
                                        name="contraseña"
                                    />
                                </div>


                                <Button type="submit" color="primary">
                                    Iniciar
                                </Button>
                            </form>
                        </div>



                    </div>
                </div>
            </Layouts>
        </>
    )
}

export default Login