import React from 'react'

import { NextUIProvider } from "@nextui-org/react";

import '@/styles/globals.css'
import SideBar from '../components/sidebar/SideBar';
import BooksState from '../context/books/booksState';
import { useRouter } from 'next/router';


const MyApp = ({ Component, pageProps }) => {
    const router = useRouter();
    return (
        <NextUIProvider>
            <BooksState>
                {
                    router.pathname !== '/login' ? (
                        <div className="flex">
                            <SideBar />
                            <Component {...pageProps} />
                        </div>
                    ) : (
                        <Component {...pageProps} />
                    )
                }


            </BooksState>
        </NextUIProvider>
    )
}

export default MyApp