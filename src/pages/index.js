import React, { useEffect, useContext, useState } from 'react';
import Layouts from '../components/Layouts'
import Footer from '../components/Footer';
import CountBooks from '../components/graphics/counting/CountBooks';
import styles from '@/styles/dashboard/index.module.css'
import BooksContext from '../context/books/booksContext';
import { Card } from "@nextui-org/react";
const index = () => {
    const { books, getAllBooks } = useContext(BooksContext)

    useEffect(() => {
        getAllBooks()
    }, [])
    console.log(books);
    const datos = [
        { id: 1, name: 'Libros totales', tamaño: books.length, color: 'bg-blue-500' },
        { id: 2, name: 'Libros disponibles', tamaño: books.length, color: 'bg-green-500' },
        { id: 3, name: 'Libros prestados', tamaño: books.length, color: 'bg-red-500' }
    ]

    return (
        <>
            <Layouts
                title={'Home'}
                description={'Esta es la pagina de inicio'}
            >

                <h1 className='text-large mb-2'>DashBoard</h1>

                <div className={styles.sectionFirst}>
                    <div className={styles.information}>
                        <h4>Estudiante destacado</h4>
                        <p>Jose Pablo</p>
                    </div>
                    <div className="grid grid-cols-3 w-full gap-10 bg-transparent">
                        <CountBooks
                            datos={datos}
                        />
                    </div>
                </div>

            </Layouts>
        </>
    )
}

export default index