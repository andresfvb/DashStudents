import DataBaseUsers from "@/data/dbUser"

const handler = async (request, response) => {
    const db = new DataBaseUsers()
    if (request.method === 'POST') {
        const lastUser = await db.login(request.body)
        return response.status(201).json({ estado: lastUser })
    } else if (request.method === 'GET') {
        const allEntries = await db.getAll()
        return response.status(200).json(allEntries)
    } else if (request.method === 'PUT') {

        const allEntries = await db.updateData(request.body)

        return response.status(200).json(allEntries)
    } else if (request.method === 'DELETE') {
        const allEntries = await db.deleteData(request.body)
        return response.status(200).json(allEntries)
    }
    return response.status(400).json({ error: 'El metodo no existe' })
}


export default handler