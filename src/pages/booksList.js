import React, { useEffect, useContext, useState } from 'react'
import Layouts from '../components/Layouts'
import FormRegister from '../components/form/FormRegister'
import styles from '@/styles/registerStudents.module.css'
import Frame from '../components/frame/Frame'
import TablesUsers from '../components/tables/TablesBooks'
import BooksContext from '../context/books/booksContext'

const RegisterBooks = () => {
    const [cargar, setCargar] = useState({ name: '', editorial: '', author: '', cod: '', id: '' })
    return (
        <>
            <Layouts
                title={'Libros'}
                description={'Lista de libros'}
            >
                <div>
                    <h1 className='text-large mb-2'>Lista de Libros</h1>
                </div>
                <div className='flex'>
                    <Frame
                        title='DashLibros'
                        Component={TablesUsers}
                        carga={cargar}
                        setCarga={setCargar}
                    />
                </div>

            </Layouts>

        </>
    )
}

export default RegisterBooks