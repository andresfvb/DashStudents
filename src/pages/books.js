import React from 'react'

const Books = () => {
    return (
        <>
            <Layouts
                title={'Libros'}
                description={'Esta es la pagina de libros'}
            >
                <div>
                    <h1>Libros</h1>
                </div>
            </Layouts>
        </>
    )
}

export default Books