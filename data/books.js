const allBooks = [
    {
        nameBook: 'Don quijote de la mancha',
        autor: 'Miguel de Cervantes',
        editorial: 'Los tres editores',
        codLibro: 'bJY7kF-3',
        state: 'disponible',
        id: 1
    },
    {
        nameBook: 'El principito',
        autor: 'Antonie de SAint-Exupery',
        editorial: 'Alfaomega Colombiana',
        codLibro: 'nkE7nr-9',
        state: 'disponible',
        id: 2
    },
    {
        nameBook: 'La culpa es de la vaca',
        autor: 'Jaime Lopera',
        editorial: 'Angosta Editores',
        codLibro: 'PoTnm_-9',
        state: 'disponible',
        id: 3
    },
]

export default allBooks