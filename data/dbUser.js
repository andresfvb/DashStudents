import allUsers from "./users"

class DataBaseUsers {
    async getAll() {
        return allBooks
    }

    async getById(id) {
        if (!allBooks.hasOwnProperty(id)) {
            return null
        }
        const entry = allBooks[id]
        return entry

    }

    async login(data) {
        const info = JSON.parse(data)

        const search = allUsers.findIndex(element => element.nameUser === info.name)
        console.log(search);
        if (search < 0) {
            return 'usuario incorrectos'
        } else {
            const valid = allUsers[search].contraseña === info.password
            if (!valid) {
                return 'Contraseña erronea'
            }
            allUsers[search].login = true
            console.log(allUsers);
            return 'Usuario Logueado'
        }
    }

    async updateData(data) {
        const info = JSON.parse(data)
        const dataNueva = allBooks.findIndex(element => element.codLibro === info.codLibro)
        allBooks[dataNueva] = info
        return allBooks.find(element => element.numero_id === info.numero_id)

    }
    async deleteData(data) {
        console.log("ENTRO ACA")
        const info = JSON.parse(data)
        const dataNueva = allBooks.findIndex(element => element.codLibro === info.codLibro)
        allBooks.splice(dataNueva, 1)
        return allBooks.find(element => element.numero_id === info.numero_id)
    }
}

export default DataBaseUsers
